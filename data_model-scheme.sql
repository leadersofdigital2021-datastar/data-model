--
-- ER/Studio Data Architect SQL Code Generation
-- Project :      db-model-data.DM1
--
-- Date Created : Saturday, September 04, 2021 15:25:26
-- Target DBMS : PostgreSQL 9.x
--

-- 
-- TABLE: dict_okpd2 
--

CREATE TABLE dict_okpd2(
    okpd2_id      serial          NOT NULL,
    okpd2_name    varchar(100)    NOT NULL,
    CONSTRAINT "PK3" PRIMARY KEY (okpd2_id)
)
;



-- 
-- TABLE: dict_suppliers 
--

CREATE TABLE dict_suppliers(
    supplier_id      serial           NOT NULL,
    supplier_name    varchar(1000)    NOT NULL,
    supplier_ogrn    varchar(30)      NOT NULL,
    CONSTRAINT "PK4" PRIMARY KEY (supplier_id)
)
;



-- 
-- TABLE: import_file_header 
--

CREATE TABLE import_file_header(
    file_id           serial          NOT NULL,
    file_orig_name    varchar(255)    NOT NULL,
    loaded_dt         timestamp       DEFAULT NOW() NOT NULL,
    processed_dt      timestamp,
    processed_flag    int2            DEFAULT 0 NOT NULL,
    CONSTRAINT "PK1" PRIMARY KEY (file_id)
)
;



-- 
-- TABLE: import_file_rows 
--

CREATE TABLE import_file_rows(
    file_row_id      serial            NOT NULL,
    file_id          int4              NOT NULL,
    fz               varchar(10)       NOT NULL,
    country_code     varchar(10),
    region_code      varchar(10),
    date_purchase    date              NOT NULL,
    purchase_name    varchar(2000),
    lot_price        numeric(10, 2),
    ktru_code        varchar(20),
    ktru_name        varchar(200),
    description      varchar(2000),
    CONSTRAINT "PK2" PRIMARY KEY (file_row_id)
)
;



-- 
-- TABLE: purchases_okdp2 
--

CREATE TABLE purchases_okdp2(
    purchases_okdp2_id    serial    NOT NULL,
    file_row_id           int4      NOT NULL,
    okpd2_id              int4      NOT NULL,
    CONSTRAINT "PK6" PRIMARY KEY (purchases_okdp2_id)
)
;



-- 
-- TABLE: purchases_suppliers 
--

CREATE TABLE purchases_suppliers(
    purchase_supplier_id    serial       NOT NULL,
    file_row_id             int4         NOT NULL,
    supplier_id             int4         NOT NULL,
    in_mailing_list         int2         DEFAULT 1 NOT NULL,
    email_sended_dt         timestamp,
    answer_received_dt      timestamp,
    CONSTRAINT "PK7" PRIMARY KEY (purchase_supplier_id)
)
;



-- 
-- TABLE: suppliers_okpd2 
--

CREATE TABLE suppliers_okpd2(
    supplier_okpd2_id    serial    NOT NULL,
    supplier_id          int4      NOT NULL,
    okpd2_id             int4      NOT NULL,
    CONSTRAINT "PK5" PRIMARY KEY (supplier_okpd2_id)
)
;



-- 
-- TABLE: import_file_rows 
--

ALTER TABLE import_file_rows ADD CONSTRAINT "Refimport_file_header1" 
    FOREIGN KEY (file_id)
    REFERENCES import_file_header(file_id) ON DELETE CASCADE ON UPDATE CASCADE
;


-- 
-- TABLE: purchases_okdp2 
--

ALTER TABLE purchases_okdp2 ADD CONSTRAINT "Refimport_file_rows4" 
    FOREIGN KEY (file_row_id)
    REFERENCES import_file_rows(file_row_id) ON DELETE CASCADE ON UPDATE CASCADE
;

ALTER TABLE purchases_okdp2 ADD CONSTRAINT "Refdict_okpd25" 
    FOREIGN KEY (okpd2_id)
    REFERENCES dict_okpd2(okpd2_id)
;


-- 
-- TABLE: purchases_suppliers 
--

ALTER TABLE purchases_suppliers ADD CONSTRAINT "Refimport_file_rows6" 
    FOREIGN KEY (file_row_id)
    REFERENCES import_file_rows(file_row_id)
;

ALTER TABLE purchases_suppliers ADD CONSTRAINT "Refdict_suppliers7" 
    FOREIGN KEY (supplier_id)
    REFERENCES dict_suppliers(supplier_id) ON DELETE CASCADE ON UPDATE CASCADE
;


-- 
-- TABLE: suppliers_okpd2 
--

ALTER TABLE suppliers_okpd2 ADD CONSTRAINT "Refdict_suppliers2" 
    FOREIGN KEY (supplier_id)
    REFERENCES dict_suppliers(supplier_id) ON DELETE CASCADE ON UPDATE CASCADE
;

ALTER TABLE suppliers_okpd2 ADD CONSTRAINT "Refdict_okpd23" 
    FOREIGN KEY (okpd2_id)
    REFERENCES dict_okpd2(okpd2_id) ON DELETE CASCADE ON UPDATE CASCADE
;


